<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


// Route::get('hello', function () {
//     return "hello world";
// });


Route::get('hello', function () {
    return view('hello');
});


// Route::get('student/{id}', function ($id) {
//     return "hello student: ".$id;
// });

Route::get('student/{id?}', function ($id="no student provided") {
         return "hello student: ".$id;
 });


 
Route::get('comment/{id?}', function ($id="no comment provided") {
         return view ('comment', ['id'=>$id]);
 });

 Route::resource('todos', 'TodoController');

 //Route::get('customers/{id?}', function ($id="no customers provided") {
 //    if ($id=="no customers provided"){
 //       return view ('comment', ['id'=>$id]);    
 //   }
 //  else{ 
 //      return view ('customers', ['id1'=>$id]);
 //  }
//});


Route::get('customers/{id?}', function ($id="no customers provided") {
    return view ('customers', ['id1'=>$id]);
});